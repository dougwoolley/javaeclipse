/***********************************************************************
Program Name:        MyLabTest.java
Programmer's Name:   Doug Woolley
Program Description: This program will test a class.
					 Within the main method, the program will create an
					 instance of a GUI object of the MyLab.java program.
***********************************************************************/

package week2tabs;

public class MyLabTest
{
	public static void main(String args[])
	{
		MyLab myLab = new MyLab();    // instantiate myLab
	}
} 

