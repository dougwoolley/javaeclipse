/***********************************************************************
Program Name:        DateTest.java
Programmer's Name:   Doug Woolley
Program Description: This program will test the Date class.
					 Within the main method, the program will 
					 instantiate a GUI object using MyLab3.java.
 ***********************************************************************/

package week3Date;

public class DateTest

{ // start of class DateTest

	public static void main(String args[])

	{ // start of main() method

		MyLab3 myLab3 = new MyLab3(); // instantiate MyLab3

	} // end of main() method

} // end of class DateTest

